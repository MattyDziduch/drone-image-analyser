import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

// Import the library
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { HeaderComponent } from './header/header.component';

@NgModule({
  imports:      [ BrowserModule, FormsModule, NgxImageZoomModule.forRoot() ],
  declarations: [ AppComponent,  HeaderComponent],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
