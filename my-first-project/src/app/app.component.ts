import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { switchMap, takeUntil, pairwise } from 'rxjs/operators';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('canvas1', { static: false }) canvas1: ElementRef;
  @ViewChild('myCanvas', { static: false }) myCanvas: ElementRef;

  //Loads of public variables, used for testing purposes ONLY
  public width = 600;
  public height = 600;

  public currentVideo = 0;
  public filesAmount;


  private cx: CanvasRenderingContext2D;
  img = new Image();
  urls = [];
  constructor() { }
  //Initialize 
  init() {
    const myCanvas1: HTMLCanvasElement = this.myCanvas.nativeElement;
    this.cx = myCanvas1.getContext('2d');

    myCanvas1.width = this.width;
    myCanvas1.height = this.height;

    this.cx.lineWidth = 3;
    this.cx.lineCap = 'round';
    this.cx.strokeStyle = '#FF0000';

    this.captureEvents(myCanvas1);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.init();
  }
  //File Chooser Code
  onSelectFile(event) {
    const canvas1: HTMLCanvasElement = this.canvas1.nativeElement;
    const ctx1: CanvasRenderingContext2D = canvas1.getContext('2d');

    if (event.target.files && event.target.files[0]) {
      this.filesAmount = event.target.files.length;
      for (let i = 0; i < this.filesAmount; i++) {
        var reader = new FileReader();

        reader.onload = (event: any) => {
          console.log(event.target.result);
          this.urls.push(event.target.result);
        }
        reader.readAsDataURL(event.target.files[i]);


      }
    }
  }
  drawImage(ctx, img) {
    img.onload = () => {
      ctx.drawImage(img, 0, 0);

    };


  }
  //Listener for the next Button
  next() {
    const canvas1: HTMLCanvasElement = this.canvas1.nativeElement;
    const ctx1: CanvasRenderingContext2D = canvas1.getContext('2d');
  
    if(this.currentVideo == 0 )
    {
      ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
      this.currentVideo++;
    this.img.src = this.urls[0];
    this.drawImage(ctx1, this.img);
    }
    else if(this.currentVideo < this.filesAmount)
    {
      ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
      
      this.img.src = this.urls[this.currentVideo];
      this.drawImage(ctx1, this.img);
      this.currentVideo++;
    }
  }
  //Listener for the previous Button
  previous() {
    const canvas1: HTMLCanvasElement = this.canvas1.nativeElement;
    const ctx1: CanvasRenderingContext2D = canvas1.getContext('2d');
    ctx1.clearRect(0, 0, canvas1.width, canvas1.height);

    if (this.currentVideo > 0 ) {
      ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
      this.img.src = this.urls[this.currentVideo--];
      this.drawImage(ctx1, this.img);
    }
    else { }
    
  }



  setCanvasSize(height, width) {
    this.height = height;
    this.width = width;
  }



  private captureEvents(myCanvas1: HTMLCanvasElement) {
    fromEvent(myCanvas1, 'mousedown')
      .pipe(
        switchMap((e) => {
          return fromEvent(myCanvas1, 'mousemove')
            .pipe(
              takeUntil(fromEvent(myCanvas1, 'mouseup')),
              takeUntil(fromEvent(myCanvas1, 'mouseleave')),
              pairwise() /* Return the previous and last values as array */
            )
        })
      ).subscribe((res: [MouseEvent, MouseEvent]) => {
        const rect = myCanvas1.getBoundingClientRect();


        const currentPos = {
          x: res[1].clientX - rect.left,
          y: res[1].clientY - rect.top
        };

        this.drawOnCanvas(currentPos);
      });
  }

  private drawOnCanvas(currentPos: { x: number, y: number }) {
    if (!this.cx) { return; }

    this.cx.beginPath();

    this.cx.arc(currentPos.x, currentPos.y, 5, 0, 2 * Math.PI);
    this.cx.stroke();
  }

}